<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Author;
use App\Catalog;
use App\Publisher;

class DataController extends Controller
{
    public function authors()
    {
        return datatables()->of(Author::query())->toJson();
    }

    public function publisher()
    {
        return datatables()->of(Publisher::query())->toJson();
    }

    public function catalog()
    {
        return datatables()->of(Catalog::query())->toJson();
    }
}
