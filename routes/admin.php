<?php

Route::get('/', 'HomeController@index')->name('dashboard');


// ROUTE AUTHOR
Route::get('/author', 'AuthorController@index')->name('author.index');

Route::get('/author/create', 'AuthorController@create')->name('author.create');

Route::post('/author', 'AuthorController@store')->name('author.store');


// ROUTE PUBLISHER
Route::get('/publisher', 'PublisherController@index')->name('publisher.index');

Route::get('/publisher/create', 'PublisherController@create')->name('publisher.create');

Route::post('/publisher', 'PublisherController@store')->name('publisher.store');


// ROUTE CATALOG
Route::get('/catalog', 'CatalogController@index')->name('catalog.index');

Route::get('/catalog/create', 'CatalogController@create')->name('catalog.create');

Route::post('/catalog', 'CatalogController@store')->name('catalog.store');



// ROUTE DATA

Route::get('/author/data', 'Datacontroller@authors')->name('author.data');

Route::get('/publisher/data', 'Datacontroller@publisher')->name('publisher.data');

Route::get('/catalog/data', 'Datacontroller@catalog')->name('catalog.data');



