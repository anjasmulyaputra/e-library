<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('author_id');
            $table->unsignedBigInteger('publisher_id');
            $table->unsignedBigInteger('catalog_id');
            $table->unsignedBigInteger('format_id');
            $table->string('title');
            $table->text('description');
            $table->string('cover');
            $table->timestamps();

            $table->foreign('author_id')->references('id')->on('authors')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

            $table->foreign('publisher_id')->references('id')->on('publishers')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

            $table->foreign('catalog_id')->references('id')->on('catalogs')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

            $table->foreign('format_id')->references('id')->on('formats')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
