@extends('admin.templates.default')

@section('content')



<div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card overflow-hidden">
							<div class="card-body border-bottom">
                                <h2 class="card-title mb-0">Publisher</h2>
							</div>
							<div class="row justify-content-center bg-light p-3">
								<div class="col-md-12">
									<div class="card shadow-sm">
										<div class="p-4 text-center">
											<div class="table-responsive">
                                            <form action="{{ route('admin.publisher.store')}}" method="post">
            @csrf

            <div class="form-group">
                <label for="">Name</label>
                <input type="text" name="name" class="form-control" placeholder="Input Name">

            </div>

            <div class="form-group">
                <input type="submit" value="Add" class="btn btn-primary">

            </div>

        </form>
			                                </div>
										</div>	
									</div>
								</div>
                            </div>
                            
						</div>
                    </div>
                </div>
            </div>



@endsection