@extends('admin.templates.default')

@section('content')


<div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card overflow-hidden">
							<div class="card-body border-bottom">
                                <h2 class="card-title mb-0">Publihers</h2>
                                <a href="{{ route ('admin.publisher.create')}}" class="btn btn-primary" style="float: right;">Add Publisher</a>
							</div>
							<div class="row justify-content-center bg-light p-3">
								<div class="col-md-12">
									<div class="card shadow-sm">
										<div class="p-4 text-center">
											<div class="table-responsive">
			                                    <table id="zero_config" class="table table-striped table-bordered no-wrap">
			                                        <thead>
			                                            <tr>
			                                                <th>Id</th>
			                                                <th>Name</th>
			                                            </tr>
			                                        </thead>
			                                        <!-- <tbody>
			                                            <tr>
			                                                <td>1</td>
			                                                <td>Anjas Mulya Putra</td>
			                                            </tr>
			                                        </tbody> -->
			                                        <tfoot>
			                                            <tr>
			                                                <th>Id</th>
			                                                <th>Name</th>
			                                            </tr>
			                                        </tfoot>
			                                    </table>
			                                </div>
										</div>	
									</div>
								</div>
                            </div>
                            
						</div>
                    </div>
                </div>
            </div>

@endsection

@push('scripts')

    <script>
        $(function () {
            $('#zero_config').dataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('admin.publisher.data') }}',
                bDestroy: true, 
                // ke konfigurasi untuk memastikan tabel data yang sudah ada dihapus sebelum diinisialisasi ulang.
                columns: [
                    { data: 'id'},
                    { data: 'name'},
                
                ]
            });
        });
    </script>

@endpush